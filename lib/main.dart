import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Loadmore Demo',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      home: MyHomePage(title: 'Loadmore Demo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int currentValue = 0;
  int perPageValue = 15;

  final originalItems = List<String>.generate(100, (i) => "Item $i");
  var items = <String>[];

  @override
  void initState() {
    super.initState();
    setState(() {
      items.addAll(
          originalItems.getRange(currentValue, currentValue + perPageValue));
      currentValue = currentValue + perPageValue;
    });
  }

  void loadMore() {
    setState(() {
      if ((currentValue + perPageValue) > originalItems.length) {
        items
            .addAll(originalItems.getRange(currentValue, originalItems.length));
      } else {
        items.addAll(
            originalItems.getRange(currentValue, currentValue + perPageValue));
      }
      currentValue = currentValue + perPageValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: NotificationListener<ScrollNotification>(
        onNotification: (ScrollNotification scrollInfo) {
          if (scrollInfo.metrics.pixels == scrollInfo.metrics.maxScrollExtent) {
            loadMore();
          }
          return false;
        },
        child: ListView.builder(
          itemCount: (currentValue <= originalItems.length)
              ? items.length + 1
              : items.length,
          itemBuilder: (context, index) {
            return (index == items.length)
                ? Container(
                    color: Colors.greenAccent,
                    child: ElevatedButton(
                      child: const Text("Load More"),
                      onPressed: () {
                        loadMore();
                      },
                    ),
                  )
                : ListTile(
                    title: Container(
                      height: MediaQuery.of(context).size.height / 10,
                      color: Colors.blueGrey.withOpacity(0.7),
                      child: Center(
                        child: Text(
                          'Loadmore Demo ${items[index]}',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white,
                          fontSize: MediaQuery.of(context).size.height / 40),
                        ),
                      ),
                    ),
                  );
          },
        ),
      ),
    );
  }
}
